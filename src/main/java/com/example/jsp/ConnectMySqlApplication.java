package com.example.jsp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ConnectMySqlApplication {

	public static void main(String[] args) {
		SpringApplication.run(ConnectMySqlApplication.class, args);
	}
}
